
//. A non-empty array of length n is called an array of all possibilities, if it contains all numbers between 0 and n - 1
//inclusive. Write a method named isAllPossibilities that accepts an integer array and returns 1 if the array is an array of all
//possibilities, otherwise it returns 0. Examples {1, 2, 0, 3} is an array of all possibilities, {3, 2, 1, 0} is an array of all possibilities, {1, 2, 4,
//        3} is not an array of all possibilities, (because 0 not included and 4 is too big), {0, 2, 3} is not an array of all possibilities, (because 1 is
//not included), {0} is an array of all possibilities, {} is not an array of all possibilities (because array is empty).
//If you are programming in Java or C#, the function signature is
//int isAllPossibilities(int[ ] a)
//If you are programming in C or C++, the function signature is
//int isAllPossibilities(int a[ ], int len)
//where len is the number of elements in the array.


public class ArrayOfPossibilities {
    public static void main(String[] args) {
        int[] arr1={1, 2, 3, 0};
        int[] arr2={};
        int[] arr3={0};
        int[] arr4={1, 2, 4,3};
        System.out.println(IsArrayOfPossibilities(arr1));
        System.out.println(IsArrayOfPossibilities(arr2));
        System.out.println(IsArrayOfPossibilities(arr3));
        System.out.println(IsArrayOfPossibilities(arr4));

    }

    private static int IsArrayOfPossibilities(int[] arr1) {
        int arrLen= arr1.length;
        if(arrLen< 1){
            return 0;
        }

        for (int i = 0; i < arrLen; i++) {
            if(!isContainPossibilities(i,arr1)){
               return 0;
            }

        }
        return 1;
    }

    private static boolean isContainPossibilities(int target, int[] arr1) {
        for (int i = 0; i < arr1.length; i++) {
            if(arr1[i] == target){
                return true;
            }

        }
        return false;

    }
}
