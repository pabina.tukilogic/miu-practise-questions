//. Mode is the most frequently appearing value. Write a function
//named hasSingleMode that takes an array argument and returns 1 if the mode value in its
//array argument occurs exactly once in the array, otherwise it returns 0. If you are writing in
//Java or C#, the function signature is
//int hasSingleMode(int[ ] ).
//If you are writing in C or C++, the function signature is
//int hasSingleMode(int a[ ], int len)
//where len is the length of a.
//        Examples
//Array elements Mode values Value returned Comments
//{1, -29, 8, 5, -29, 6 -29} 1 single mode
//{1, 2, 3, 4, 2, 4, 7 2, 4}0 no single mode
//{1, 2, 3, 4, 6 1, 2, 3, 4,6} 0 no single mode
//{7, 1, 2, 1, 7, 4, 2, 7, 7}1 single mode

public class SingleModeArray {
    public static void main(String[] args) {
        int [] a={1, -29, 8, 5, -29,6};
        int [] a1={1, 2, 3, 4, 2, 4, 7};
        int [] a2={1, 2, 3, 4, 6,1, 2, 3, 4,6};
        int [] a3={7, 1, 2, 1, 7, 4, 2, 7,};
        System.out.println(hasSingleMode(a));
        System.out.println(hasSingleMode(a1));
        System.out.println(hasSingleMode(a2));
        System.out.println(hasSingleMode(a3));

    }

    private static int hasSingleMode(int[] a) {
        int isSingleModeCount=0;
        for (int i = 0; i < a.length ; i++) {
            for (int j = 0; j <i ; j++) {
                if(a[i]==a[j]){
                    isSingleModeCount ++;
                }

            }

        }
        if(isSingleModeCount != 1){
            return 0;
        }
        return 1;
    }
}
