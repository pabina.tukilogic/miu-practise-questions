
//A Meera array is defined to be an array such that for all values n in the array, the value 2*n is not in
//the array. So {3, 5, -2} is a Meera array because 3*2, 5*2 and -2*2 are not in the array. But {8, 3, 4} is
//not a Meera array because for n=4, 2*n=8 is in the array.
//Write a function named isMeera that returns 1 if its array argument is a Meera array. Otherwise it
//returns 0.
//If you are programming in Java or C#, the function signature is
//int isMeera(int [ ] a)
//If you are programming in C or C++, the function signature is
//int isMeera(int a[ ], int len) where len is the number of elements in the array.

public class MeeraArray {
    public static void main(String[] args) {
        int[] arr1={3, 5,-2};
        int[] arr2={4, 3, 8};
        System.out.println(isMeera(arr1));
        System.out.println(isMeera(arr2));

    }

    private static int isMeera(int[] arr1) {
        int arrLength= arr1.length;
        for (int i = 0; i <arrLength ; i++) {
            for (int j = 0; j < arrLength; j++) {
                if(arr1[i]*2== arr1[j]){
                    return 0;
                }

            }

        }
        return 1;
    }
}
