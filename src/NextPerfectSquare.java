//Question 7
//Write a function nextPerfectSquare that returns the first perfect square that is greater than
//it's integer argument. A perfect square is an integer that is equal to some integer squared.
//
//For example 16 is a perfect square because 16 = 4*4. However 15 is not a perfect square because
//there is no integer n such that 15 = n*n.
//
//The signature of the function is
//int isPerfectSquare(int n)
//Examples
// -------------------------|--------------------------------------------------------------
//         | n                       | next perfect square                                          |
//        |-------------------------|--------------------------------------------------------------|
//        | 6                       | 9                                                            |
//        |-------------------------|--------------------------------------------------------------|
//        | 36                      | 49                                                           |
//        |-------------------------|--------------------------------------------------------------|
//        | 0                       | 1                                                            |
//        |-------------------------|--------------------------------------------------------------|
//        | -5                      | 0                                                            |
//        -------------------------|--------------------------------------------------------------


public class NextPerfectSquare {
    public static void main(String[] args) {
        System.out.println(isPerfectSquareNew(6));
        System.out.println(isPerfectSquareNew(36));
        System.out.println(isPerfectSquareNew(0));
        System.out.println(isPerfectSquareNew(-5));
    }

    private static int isPerfectSquareNew(int n) {
        int nextNumber=n+1;
        while (!isSqureNext(nextNumber)){
            nextNumber ++;
        }
        return  nextNumber;
    }

    private static boolean isSqureNext(int n) {
        if(n== 0 || n==1){
          return true;
        }
        for (int i = 2; i < n ; i++) {
            if(n%i == 0 && n/i == i){
                return true;
            }

        }
        return  false;
    }
}
