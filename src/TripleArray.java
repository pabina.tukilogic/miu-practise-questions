//Define a Triple array to be an array where every value occurs exactly three times.
//For example, {3, 1, 2, 1, 3, 1, 3, 2, 2} is a Triple array.
//The following arrays are not Triple arrays
//{2, 5, 2, 5, 5, 2, 5} (5 occurs four times instead of three times)
//        {3, 1, 1, 1} (3 occurs once instead of three times)
//Write a function named isTriple that returns 1 if its array argument is a Triple array. Otherwise it returns 0.
//If you are programming in Java or C#, the function signature is
//int isTriple (int[ ] a)
//If you are programming in C or C++, the function signature is
//int isTriple (int a[ ], int len) where len is the number of elements in the array.


public class TripleArray {
    public static void main(String[] args) {
        int[] myarr1 = {3, 1, 2, 1, 3, 1, 3, 2, 2};
        int[] myarr2 = {2, 5, 2, 5, 5, 2, 5};
        int[] myarr3 =  {3, 1, 1, 1};
        System.out.println(isTriple(myarr1));
        System.out.println(isTriple(myarr2));
        System.out.println(isTriple(myarr3));

    }

    private static int isTriple(int[] myarr1) {

        for (int i = 0; i < myarr1.length; i++) {
            if(!isTripleCountFun(myarr1[i],myarr1)){
                return 0;
            }

        }

        return 1;


    }

    private static boolean isTripleCountFun(int n,int[] arr1) {

        int tripleCount= 0;
        for (int j = 0; j < arr1.length; j++) {
            if(arr1[j]== n){
                tripleCount++;
            }

        }
        if(tripleCount == 3){
            return true;
        }
        return false;
    }
}
