
//Write a function named countDigit that returns the number of times that a given digit appears in a
//positive number. For example countDigit(32121, 1) would return 2 because there are two 1s in 32121.
//Other examples:
//countDigit(33331, 3) returns 4
//countDigit(33331, 6) returns 0
//countDigit(3, 3) returns 1
//The function should return -1 if either argument is negative, so
//countDigit(-543, 3) returns -1.
//The function signature is
//int countDigit(int n, int digit)
//Hint: Use modulo base 10 and integer arithmetic to isolate the digits of the number.

public class CountDigits {

    public static void main(String[] args) {

        System.out.println(countDigit(33331,3));
        System.out.println(countDigit(33331,6));
        System.out.println(countDigit(3,3));
        System.out.println(countDigit(-543, 3));
    }

    private static int countDigit(int number, int k) {

    int count =0;
    if(number<0){
        return  -1;
    }
    int remainning=0;
      while(number > 0){
          remainning = number%10;
          if(remainning == k){
              count++;
          }
          number= number/10;

      }
      return  count;
    }
}
