//1. Write a function named minDistance that returns the smallest distance between two factors of a
//        number. For example, consider 13013 = 1*7*11*13. Its factors are 1, 7, 11, 13 and 13013.
//minDistance(13013) would return 2 because the smallest distance between any two factors is 2 (13 -
//        11 = 2). As another example, minDistance (8) would return 1 because the factors of 8 are 1, 2, 4, 8
//and the smallest distance between any two factors is 1 (2 – 1 = 1).
//The function signature is
//int minDistance(int n)

public class MinDistance {
    public static void main(String[] args) {
        System.out.println(minDistance( 8));
        System.out.println(minDistance( 13013));
    }

    private static int minDistance(int N) {
        int distance = Integer.MAX_VALUE;
        if(N<1){
            return 0;
        }
//
//        for (int i = 1; i < N; i++) {
//
//           if (N % i == 0){
//               int currentFactor = i;
//               for (int j = i+1; j < N; j++) {
//                   if (N % j == 0){
//                       int nextFactor = j;
//                       if (nextFactor - currentFactor < distance){
//                           distance = nextFactor - currentFactor;
//                       }
//                   }
//
//               }
//
//           }
//        }


//        for (int i = 1; i < N; i++) {
//            int prefFactor = 0;
//            int currentFactor = 0;
//            if (N % i == 0 ){
//               prefFactor = i;
//            }
//
//            for (int j = i+1; j < N; j++) {
//                if (N % j == 0){
//                   currentFactor = j;
//                }
//            }
//
//            if (currentFactor - prefFactor < distance){
//                distance = currentFactor - prefFactor;
//            }
//
//
//        }
        for (int i = 1; i < N ; i++) {
            if(newFactors(i,N)){
                for (int j = i+1; j < N ; j++) {
                    if(newFactors(j,N)){
                        if(distance> j-i){
                            distance = j-i;
                        }
                    }

                }
            }

        }
        return  distance;
    }


    private static boolean newFactors(int i, int n) {
        return n%i == 0 ;
    }
}
