/**
 * @Author
 * @Created 2024/04/03
 * @Name MIU
 *
 *  * A Madhav array has the following property.
 *  * a[0] = a[1] + a[2] = a[3] + a[4] + a[5] = a[6] + a[7] + a[8] + a[9] = ...
 *  * The length of a Madhav array must be n*(n+1)/2 for some n.
 *  * <p>
 *  * Write a method named isMadhavArray that returns 1 if its array argument is a Madhav array,
 *  * otherwise it returns 0. If you are programming in Java or C# the function signature is
 *  * int isMadhavArray(int[ ] a)

 */



public class MadavArray {
    public static void main(String[] args) {
        int[] arr = {2, 1, 1};
        int[] arr1 = {2, 1, 1, 4, -1, -1};
        int[] arr2 = {6, 2, 4, 2, 2, 2, 1, 5, 0, 0};
        int[] arr3 = {18, 9, 10, 6, 6, 6};
        int[] arr4 = {-6, -3, -3, 8, -5, -4};
        int[] arr5 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, -2, -1};
        int[] arr6 = {3, 1, 2, 3, 0};

        System.out.println(isMadhavArray(arr));
        System.out.println(isMadhavArray(arr1));
        System.out.println(isMadhavArray(arr2));
        System.out.println(isMadhavArray(arr3));
        System.out.println(isMadhavArray(arr4));
        System.out.println(isMadhavArray(arr5));
        System.out.println(isMadhavArray(arr6));

    }

    private static int isMadhavArray(int[] a) {

        int start=1;
        int round=1;
        int end = start+ round;
        int lengthis=a.length;
        int isMadav = 0;


        for (int i = 0; i < a.length ; i++) {
        if(i*(i+1)/2 == lengthis){
            isMadav =1;
            break;
        }
        }


        while (end< a.length) {
            int sum =0;
            for (int i = start; i <= end; i++) {
                sum += a[i];

            }
            if (sum != a[0]) {
                return 0;
            }

            round++;
            start = end + 1;
            end = start+ round;

        }

        if(isMadav !=1){
            return 0;
        }

        return 1;


    }
}
