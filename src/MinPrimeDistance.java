
//Write a function named minPrimeDistance that returns the smallest distance
//between two prime factors of a number. For example, consider 13013.
//Its prime factors are 1, 7, 11, 13 and 13013. minPrimeDistance(13013) would
//return 2 because the smallest distance between any two prime factors is 2 (13 - 11
//        = 2). As another example, minPrimeDistance (8) would return 1 because the prime
//factors of 8 are 1, 2 and the smallest distance between any two factors is 1 (2 - 1 =
//        1). Note: Consider 1 as a prime number.
//The function signature is
//int minPrimeDistance(int n)

public class MinPrimeDistance {
    public static void main(String[] args) {
        System.out.println(minPrimeDistance(13013));
        System.out.println(minPrimeDistance(8));

    }

    private static int minPrimeDistance(int n) {
        int minDistance= Integer.MAX_VALUE;
        if(n==1){
            return 1;
        }

        for (int i = 1; i <n ; i++) {
            if(n%i == 0 && isPrimeDistance(i)){
                for (int j = i+1; j <n ; j++) {
                    if(n%j == 0 && isPrimeDistance(j)){
                        if(minDistance> j-i){
                            minDistance = j-i;
                        }
                    }
                }
            }

        }
        return  minDistance;
    }

    private static boolean isPrimeDistance(int number) {
        if(number< 1){
            return false;
        }
        for (int i = 2; i < number ; i++) {
            if(number%i == 0){
                return false;
            }

        }
        return true;
    }
}
