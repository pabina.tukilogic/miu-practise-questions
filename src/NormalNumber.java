
//. A normal number is defined to be one that has no odd factors, except for 1 and possibly itself.
//Write a method named isNormal that returns 1 if its integer argument is normal, otherwise it returns 0. The
//function signature is
//int isNormal(int n)
//Examples: 1, 2, 3, 4, 5, 7, 8 are normal numbers. 6 and 9 are not normal numbers since 3 is an odd factor. 10 is
//not a normal number since 5 is an odd factor.

public class NormalNumber {
    public static void main(String[] args) {
        System.out.println(isNormal(3));
        System.out.println(isNormal(1));
        System.out.println(isNormal(4));
        System.out.println(isNormal(5));
        System.out.println(isNormal(7));
        System.out.println(isNormal(8));
        System.out.println(isNormal(2));
        System.out.println(isNormal(6));
        System.out.println(isNormal(9));
    }

    private static int isNormal(int n) {
        if(n == 1){
            return 1;
        }
        for (int i = 2; i < n ; i++) {
            if(!isNormalFactors(i, n)){
               return 0;
            }

        }
        return 1;
    }

    private static boolean isNormalFactors(int target, int n) {
        if(n%target == 0){
            if(target%2 != 0){
                return false;
            }
        }
        return true;
    }
}
