
//An array is called balanced if its even numbered elements (a[0], a[2], etc.) are even and its odd
//numbered elements (a[1], a[3], etc.) are odd.
//Write a function named isBalanced that accepts an array of integers and returns 1 if the array is
//balanced, otherwise it returns 0.
//Examples: {2, 3, 6, 7} is balanced since a[0] and a[2] are even, a[1] and a[3] are odd. {6, 7, 2, 3, 12}
//is balanced since a[0], a[2] and a[4] are even, a[1] and a[3] are odd.
//        {7, 15, 2, 3} is not balanced since a[0] is odd.
//        {16, 6, 2, 3} is not balanced since a[1] is even.
//If you are programming in Java or C#, the function signature is
//int isBalanced(int[ ] a)
//If you are programming in C or C++, the function signature is
//int isBalanced(int a[ ], int len)
//where len is the number of elements in the array.

public class BalancedArray {

    public static void main(String[] args) {
        int[] arr1= {16, 6, 2, 3};
        int[] arr= {2, 3, 6, 7};
        System.out.println(isBalanced(arr1));
        System.out.println(isBalanceV2(arr1));
        System.out.println(isBalanced(arr));
        System.out.println(isBalanceV2(arr));
    }

    private static int isBalanceV2(int[] a) {
        for (int i = 0; i < a.length; i++) {
            if(!(a[i]%2==0 && i%2==0) && !(a[i]%2 !=0 && i%2 !=0)){
                return 0;
            }

        }
        return 1;
    }

    private static int isBalanced(int[] arr1) {
        int iseven = 0;
        int isodd= 0 ;
        for (int i = 0; i < arr1.length; i++) {
          if(i%2 == 0){
              if(arr1[i]% 2 == 0){
                  iseven =1;
              }
              else{
                  iseven =0;
                  break;
              }
          }


            if(i%2 != 0){
                if(arr1[i]% 2 != 0){
                    isodd =1;
                }
                else{
                    isodd =0;
                    break;
                }
            }
        }


        int myResult = (iseven == 1 && isodd == 1) ? 1 : 0;


        return myResult;

    }


}
