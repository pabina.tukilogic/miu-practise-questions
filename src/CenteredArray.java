
//An array with an odd number of elements is said to be centered if all elements (except the middle
//        one) are strictly greater than the value of the middle element. Note that only arrays with an odd
//number of elements have a middle element. Write a function named isCentered that accepts an
//integer array and returns 1 if it is a centered array, otherwise it returns 0.
//Examples: {5, 3, 3, 4, 5} is not a centered array (the middle element 3 is not strictly less than all other
//        elements), {3, 2, 1, 4, 5} is centered (the middle element 1 is strictly less than all other elements), {3,
//        2, 1, 4, 1} is not centered (the middle element 1 is not strictly less than all other elements), {3, 2, 1, 1,
//        4, 6} is not centered (no middle element since array has even number of elements), {} is not centered
//        (no middle element), {1} is centered (satisfies the condition vacuously).
//If you are programming in Java or C#, the function signature is
//int isCentered(int[ ] a)
//If you are programming in C or C++, the function signature is
//int isCentered(int a[ ], int len)
//where len is the number of elements in the array.
public class CenteredArray {
    public static void main(String[] args) {
        int[] a = {3, 2, 1, 1,4, 6};
        System.out.println(isCentered(a));
    }

    private static int isCentered(int[] a) {
        int middleElement =a[a.length/2];
        System.out.println(middleElement);
       if(a.length <= 0 && a.length % 2 == 0){
           return  0;
       }
        for (int i = 0; i < (a.length/2) ; i++) {
            if(middleElement>= a[i]){
                return  0;
            }

        }

        for (int i = (a.length/2)+1; i < (a.length) ; i++) {
            if(middleElement>=a[i]){
                return  0;
            }

        }
        return  1;
    }

}
