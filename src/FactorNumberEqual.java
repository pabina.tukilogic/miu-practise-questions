
//Two integers are defined to be factor equal, if they have the same number of factors. For example, integers 10 and 33
//are factor equal because, 10 has four factors: 1, 2, 5, 10 and 33 also has four factors: 1, 3, 11, 33. On the other hand, 9
//and 10 are not factor equal since 9 has only three factors: 1, 3, 9 and 10 has four factors: 1, 2, 5, 10.
//Write a function named factorEqual(int n, int m) that returns 1 if n and m are factor equal and 0 otherwise.
//The signature of the function is
//int factorEqual(int n, int m)


public class FactorNumberEqual {
    public static void main(String[] args) {


        System.out.println(factorEqual(10, 33));
        System.out.println(factorEqual(10, 9));

    }

    private static int factorEqual(int x, int y) {
        int factorOneCount = 0;
        int factorTwoCount = 0;

        if (x < 1 || y < 1) {
            return 0;
        }

        for (int i = 1; i <= x; i++) {
            if (x % i == 0) {
                factorOneCount++;

            }
        }


        for (int j = 1; j <= y; j++) {
            if (y % j == 0) {
                factorTwoCount++;

            }
        }

//        System.out.println(factorOneCount);
//        System.out.println(factorTwoCount);

        if (factorOneCount == factorTwoCount) {
            return 1;
        }





        return 0;
    }
}
