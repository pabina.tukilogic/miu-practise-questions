
//A Bunker array is an array that contains the value 1 if and only if it contains a prime number. The
//array {7, 6, 10, 1} is a Bunker array because it contains a prime number (7) and also contains a 1.
//The array {7, 6, 10} is not a Bunker array because it contains a prime number (7) but does not
//contain a 1. The array {6, 10, 1} is not a Bunker array because it contains a 1 but does not contain a
//prime number.
//It is okay if a Bunker array contains more than one value 1 and more than one prime, so the array {3,
//        7, 1, 8, 1} is a Bunker array (3 and 7 are the primes).
//Write a function named isBunker that returns 1 if its array argument is a Bunker array and returns 0
//otherwise.
//You may assume the existence of a function named isPrime that returns 1 if its argument is a prime
//and returns 0 otherwise. You do not have to write isPrime, you can just call it.
//If you are programming in Java or C#, the function signature is
//int isBunker(int [ ] a)
//If you are programming in C or C++, the function signature is
//int isBunker(int a[ ], int len) where len is the number of elements in the array.


public class BunkerArray2 {
    public static void main(String[] args) {
        int[] arr1={7, 6, 10, 1};
        int[] arr2={7, 6, 10};
        int[] arr3={6, 10, 1};
        int[] arr4={3,7, 1, 8, 1};

        System.out.println(isBunker(arr1));
        System.out.println(isBunker(arr2));
        System.out.println(isBunker(arr3));
        System.out.println(isBunker(arr4));



    }

    private static int isBunker(int[] arr1) {
        int isContainPrime=0;
        int isContain1 = 0;
        int lenghtArray= arr1.length;

        for (int i = 0; i < lenghtArray; i++) {
            if(isPrime(arr1[i])){
                isContainPrime=1;
            }
            if(arr1[i]== 1){
                isContain1 = 1;
            }

        }

        return ((isContain1 ==1 && isContainPrime==1)?1:0);
    }

    private static boolean isPrime(int i) {
        if(i<=1){
            return false;
        }
        for (int j = 2; j < i ; j++) {
            if(i%j == 0){
                return false;
            }

        }
        return true;
    }
}
