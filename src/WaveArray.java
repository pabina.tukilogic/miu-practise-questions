//A wave array is defined to an array which does not contain two even numbers or two odd
//numbers in adjacent locations. So {7, 2, 9, 10, 5}, {4, 11, 12, 1, 6}, {1, 0, 5} and {2} are all wave
//arrays. But {2, 6, 3, 4} is not a wave array because the even numbers 2 and 6 are adjacent to each
//other.
//Write a function named isWave that returns 1 if its array argument is a Wave array, otherwise it
//returns 0.
//If you are programming in Java or C#, the function signature is
//int isWave (int [ ] a)
//If you are programming in C or C++, the function signature is
//int isWave (int a[ ], int len) where len is the number of elements in the array.




public class WaveArray {
    public static void main(String[] args) {
        int [] arr1= {7, 2, 9, 10, 5};
        int [] arr2= {2, 6, 3, 4};
        System.out.println(isWave(arr2));
        System.out.println(isWave(arr1));
    }

    private static int isWave(int[] arr1) {
        int lengthArr= arr1.length;
        if(lengthArr == 1){
            return 1;
        }
        for (int i = 0; i < lengthArr -1 ; i++) {
            if((isNumberEven(arr1[i]) && isNumberEven(arr1[i+1])) ||( !isNumberEven(arr1[i]) && !isNumberEven(arr1[i+1])) ){
                return 0;
            }

        }
        return 1;
    }

    private static boolean isNumberOdd(int i) {
       return i%2 !=0;
    }

    private static boolean isNumberEven(int i) {
       return  i % 2 == 0;
    }
}
