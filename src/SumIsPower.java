
//Write a function sumIsPower with signatuare
//boolean sumIsPower(int[] arr)
//which outputs true if the sum of the elements in the input array arr is a power of 2, false otherwise. Recall that
//the powers of 2 are 1, 2, 4, 8, 16, and so on. In general a number is a power of 2 if and only if it is of the form 2
//n
//for some nonnegative integer n. You may assume (without verifying in your code) that all elements in the array
//are positive integers. If the input array arr is null, the return value should be false.
//Examples: sumIsPower({8,8,8,8}) is true since 8 + 8 + 8 + 8 = 32 = 25
//        . sumIsPower({8,8,8}) is false, since 8
//        + 8 +8 = 24, not a power of 2.

public class SumIsPower {
    public static void main(String[] args) {
        int [] a1={8,8,8,8};
        int [] a4={256,256,256};
        int [] a2 ={8,8,8};
        int [] a3 ={2,2};
        System.out.println(sumIsPower(a1));
        System.out.println(sumIsPower(a2));
        System.out.println(sumIsPower(a3));
        System.out.println(sumIsPower(a4));
    }

    private static boolean sumIsPower(int[] a) {
        int sum = 0;
        for (int i = 0; i <a.length ; i++) {
            sum +=a[i];
        }
        if(!isFactorsPower(sum)){
           return false;
        }
        return true;
    }

    private static boolean isFactorsPower(int n) {
        for (int i = 2; i < n; i++) {
            if(n%i ==0){
                if(i%2 != 0){
                   return false;
                }
            }

        }
        return true;

    }


}
