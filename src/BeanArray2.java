
// for every value there is present of 2n or n/2;
public class BeanArray2 {

    public static void main(String[] args) {
        int[] arr1={4, 8,7};
        int[] arr2={4, 8,2};
        System.out.println(isPresent(arr1));
        System.out.println(isPresent(arr2));
        System.out.println("--------------------------------");
        System.out.println(isPresentV2(arr1));
        System.out.println(isPresentV2(arr2));

    }

    private static int isPresentV2(int[] a) {
        for (int i = 0; i <a.length ; i++) {
            if(!isCondition(a[i],a)){
                return 0;
            }

        }
        return 1;
    }

    private static boolean isCondition(int i, int[] a) {
        for (int j = 0; j < a.length; j++) {
            if(a[j]== 2*i || a[j]== i/2){
                return true;
            }

        }
        return false;
    }

    private static int isPresent(int[] arr1) {
        for (int i = 0; i < arr1.length ; i++) {
            int num = arr1[i];
            boolean condition1 = isContainB(arr1,num*2);
            boolean codition2 = isContainB(arr1,num/2);
            if(!condition1 && !codition2){
               return 0;
            }

//            if(!isContainB(arr1,num*2) && !isContainB(arr1,num/2)){
//                return 0;
//            }

        }
        return 1;
    }

    private static boolean isContainB(int[] a, int target) {
        for (int j = 0; j < a.length; j++) {
            if(a[j]== target){
                return true;
            }

        }
        return false;
    }

}
