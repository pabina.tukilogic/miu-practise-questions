//An array is said to be hollow if it contains 3 or more zeroes in the middle that are preceded and
//followed by the same number of non-zero elements. Write a function named isHollow that accepts an
//integer array and returns 1 if it is a hollow array, otherwise it returns 0
//Examples: isHollow({1,2,4,0,0,0,3,4,5}) returns 1. isHollow ({1,2,0,0,0,3,4,5}) returns 0. isHollow
//        ({1,2,4,9, 0,0,0,3,4, 5}) returns 0. isHollow ({1,2, 0,0, 3,4}) returns 0.
//If you are programming in Java or C#, the function signature is
//int isHollow(int[ ] a).
//If you are C or C++ programmer
//int isHollow(int[ ] a, int len)
//where len is the number of elements in the array





public class HollowArray {
    public static void main(String[] args) {
        int [] arr1= {1,2,3,0,0,0,0,0,6,3,9};
        int [] arr2= {1,0,0,0,2,0,0,0,9};
        System.out.println(isHollow(arr2));


    }

    private static int isHollow(int[] a) {
        int firstCount=0;
        int lastCount =0;
        int zeroCount = 0;
        int zeroInclude = 0;


        for (int i = 0; i <a.length ; i++) {
            if(a[i] == 0){
                zeroCount++;
            }
        }


        for (int i = 0; i < a.length; i++) {
            if(a[i] != 0){
               firstCount ++;
            }
            else{
                break;
            }
        }


        for (int i =a.length-1; i > 0; i--) {
            if(a[i] != 0){
                lastCount++;
            }
            else{
                break;
            }
        }








//        System.out.println(firstCount);
//        System.out.println(lastCount);
        System.out.println(firstCount-1);
        System.out.println(a.length-lastCount);
        System.out.println("zeroInclude"+zeroInclude);
        System.out.println(zeroCount);


        for (int i = (firstCount-1); i <(a.length-lastCount); i++) {
            if(a[i] > 0){
                zeroInclude = 1;
                break;
            }

        }

        System.out.println(firstCount);
        System.out.println(lastCount);
        int firsResults = (zeroInclude==0)?1:0;
        System.out.println("firstResult"+firsResults);


       int myResults = (zeroCount >= 3 && lastCount ==firstCount || zeroInclude != 1 )?1:0;
//        int myResults = (zeroCount >= 3 && lastCount == firstCount  )?1:0;
        System.out.println(myResults);
       return  myResults;
    }
}
