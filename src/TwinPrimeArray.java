
//Consider the prime number 11. Note that 13 is also a prime and 13 – 11 = 2. So, 11 and 13 are
//known as twin primes. Similarly, 29 and 31 are twin primes. So is 71 and 73. However, there are
//many primes for which there is no twin. Examples are 23, 67. A twin array is defined to an array
//which every prime that has a twin appear with a twin. Some examples are
//{3, 5, 8, 10, 27}, // 3 and 5 are twins and both are present
//        {11, 9, 12, 13, 23}, // 11 and 13 are twins and both are present, 23 has no twin
//        {5, 3, 14, 7, 18, 67}. // 3 and 5 are twins, 5 and 7 are twins, 67 has no twin
//The following are NOT twin arrays:
//        {13, 14, 15, 3, 5} // 13 has a twin prime and it is missing in the array
//        {1, 17, 8, 25, 67} // 17 has a twin prime and it is missing in the array
//Write a function named isTwin(int[ ] arr) that returns 1 if its array argument is a Twin array, otherwise
//it returns 0.


public class TwinPrimeArray {
    public static void main(String[] args) {
        int[] arr1={3, 5, 8, 10, 27};
        int[] arr2={11, 9, 12, 13, 23};
        int[] arr3={5, 3, 14, 7, 18, 67};
        int[] arr4={13, 14, 15, 3, 5};
        int[] arr5={1, 17, 8, 25, 67};
        System.out.println(isTwin(arr1));
        System.out.println(isTwin(arr2));
        System.out.println(isTwin(arr3));
        System.out.println(isTwin(arr4));
        System.out.println(isTwin(arr5));

    }

    private static int isTwin(int[] arr1) {
        int arrLen= arr1.length;
        for (int i = 0; i < arrLen; i++) {

            if(isPrime(arr1[i]) && (isPrime(arr1[i]+2) || isPrime(arr1[i]-2))){
                if(!(hasInclude((arr1[i]+2),arr1)   && !(hasInclude((arr1[i]-2),arr1)))){
                    return 0 ;
                }
//                if (!(hasInclude(arr1[i] + 2, arr1) || !(hasInclude(arr1[i] - 2, arr1)))) {
//                    return 0;
//                }


            }

        }
        return 1;
    }

    private static boolean hasInclude(int i, int[] arr1) {
        for (int j = 0; j < arr1.length; j++) {
            if(arr1[j]== i){
                return true;
            }

        }
        return false;
    }


    private static boolean isPrime(int x) {
        if(x <= 1){
            return  false;
        }
        for(int i=2; i< x ; i++){
            if(x% i == 0){
                return  false;
            }
        }
        return true;
    }
}
