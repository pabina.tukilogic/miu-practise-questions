
//Let us define two arrays as “set equal” if every element in one is also in the other and vice-versa.
//        For example, any two of the following are equal to one another: {1, 9, 12}, {12, 1, 9}, {9, 1, 12, 1}, {1,
//        9, 12, 9, 12, 1, 9}. Note that {1, 7, 8} is not set equal to {1, 7, 1} or {1, 7, 6}.
//Write a function named isSetEqual(int[ ] a, int[ ] b) that returns 1 if its array arguments are set equal,
//otherwise it returns 0.

public class SetEqual {
    public static void main(String[] args) {
        int [] arr1={1, 9, 12};
        int [] arr2 ={12, 1, 9};
        int [] arr3 = {9, 1, 12, 1};
        int [] arr4 ={1, 9, 12, 9, 12, 1, 9};
        int [] arr5 ={1, 7, 8};
        int [] arr6 ={1, 7, 1};
        System.out.println(isSetEqual(arr1,arr2));
        System.out.println(isSetEqual(arr3,arr4));
        System.out.println(isSetEqual(arr5,arr6));

    }

    private static int isSetEqual(int[] arr5, int[] arr6) {
        for (int i = 0; i < arr5.length ; i++) {
            if(!hasIncluded(arr5[i],arr6)){
               return 0;
            }

        }
        if(uniquerCountArray(arr5)!= uniquerCountArray(arr6)){
           return  0;
        }
        return 1;

    }

    private static boolean hasIncluded(int i, int[] arr6) {
        for (int j = 0; j < arr6.length; j++) {
            if(arr6[j] == i){
                return true;
            }

        }
        return false;
    }


    private static int uniquerCountArray(int[] arr1) {
        int uniqueCount= 0;
        for (int i = 0; i < arr1.length ; i++) {
            boolean unique= true;
            for (int j = 0; j <i ; j++) {
                if(arr1[i] == arr1[j]){
                  unique =false;
                  break;

            }}
            if(unique){
                uniqueCount++;
            }


        }
        return  uniqueCount;
    }
}
