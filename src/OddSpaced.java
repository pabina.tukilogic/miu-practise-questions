
//An integer array is said to be oddSpaced, if the difference between the largest value
//and the smallest value is an odd number. Write a function isOddSpaced(int[] a) that will
//return 1 if it isoddSpaced and 0 otherwise. If array has less than two elements, function
//will return 0. If you are programming in C or C++, the function signature is:
//int isOddSpaced (int a[ ], int len) where len is the number of elements in the array.


public class OddSpaced {
    public static void main(String[] args) {
        int [] a={1,4,6,8,9};
        int [] a1={2,4,6,8,9};
        System.out.println(isOddSpace(a));
        System.out.println(isOddSpace(a1));
    }

    private static int isOddSpace(int[] a) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < a.length; i++) {
            if (a[i] < min) {
                min = a[i];
            }
            if (a[i] > max) {
                max = a[i];
            }
        }
        if((max-min)%2 == 0){
            return 0;
        }
        return 1;
    }
}
