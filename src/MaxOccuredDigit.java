//
//Write a function named maxOccurDigit that returns the digit that occur the most. If
//there is no such digit, it will return -1. For example maxOccurDigit(327277) would return
//        7 because 7 occurs three times in the number and all other digits occur less than three
//times. Other examples:
//maxOccurDigit(33331) returns 3
//maxOccurDigit(3232, 6) returns -1
//maxOccurDigit(5) returns 5
//maxOccurDigit(-9895) returns 9
//The function signature is
//maxOccurDigit(int n)

public class MaxOccuredDigit {
    public static void main(String[] args) {
        System.out.println(maxOccurDigit(33331));
        System.out.println(maxOccurDigit(3232));
        System.out.println(maxOccurDigit(9895));

    }

    private static int maxOccurDigit(int n) {
        int maxCount = 0;
        int NumberI = 0;
        while (n>0){
            int remember = n%10;
            int rememberCount=0;
            int innerNumber= n;
             while (innerNumber>0){
                 int rememberInner= innerNumber % 10;
                 if(remember== rememberInner){
                     rememberCount ++;
                 }
                 innerNumber= innerNumber/10;
             }
             n=n/10;
             if(rememberCount> maxCount){
                 maxCount = rememberCount;
                 NumberI = remember;
             }

        }

        return  NumberI;


    }
}
