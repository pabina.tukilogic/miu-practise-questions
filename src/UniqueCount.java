public class UniqueCount {
    public static void main(String[] args) {
        int [] a ={1,2,3,4,5,2,1};
        int [] a1 ={22,19,10,10,19,22,22,10};
        int [] a2 ={-1,0,1,0,0,0};
        int [] a4 ={2147483647,-1,-1,-2147483648};
        int [] a5 ={11,11,2,6};


        System.out.println(uniquerCountArray(a));
        System.out.println(uniquerCountArray(a1));
        System.out.println(uniquerCountArray(a2));
        System.out.println(uniquerCountArray(a4));
        System.out.println(uniquerCountArray(a5));
    }

    private static int uniquerCountArray(int[] arr1) {
        int uniqueCount= 0;
        for (int i = 0; i < arr1.length ; i++) {
            boolean unique = true;
            for (int j = 0; j < i; j++) {
                if (arr1[i] == arr1[j]) {
                    unique = false;
                    break;

                }
            }
            if (unique) {
                uniqueCount++;
            }
        }

return uniqueCount;
        }
}
