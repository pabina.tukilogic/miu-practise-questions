//Question 3
//Write a function that accepts a character array, a zero-based start position and
//a length. It should return a character array containing lengthCharacters starting with
//the startCharacter of the input array. The function should do error checking on the start
//position and the length and return null if the either value is not legal.
//
//The function signature is:
//char[] f(char[] a, int start, int len)
//
// --------------------------------|--------------------------------------------------------------
//         | if the input parameters are is | return                                                       |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 0, 4            | null                                                         |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 0, 3            | {'a','b','c'}                                                |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 0, 2            | {'a','b'}                                                    |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 0, 1            | {'a'}                                                        |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 1, 3            | null                                                         |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 1, 2            | {'b','c'}                                                    |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 1, 1            | {'b'}                                                        |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 2, 2            | null                                                         |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 2, 1            | {'c'}                                                        |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 3, 1            | null                                                         |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, 1, 0            | {}                                                           |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, -1, 2           | null                                                         |
//        |--------------------------------|--------------------------------------------------------------|
//        | {'a','b','c'}, -1, -2          | null                                                         |
//        |--------------------------------|--------------------------------------------------------------|
//        | {}, 0, 1                       | null                                                         |


import java.util.Arrays;

public class CharacterArray {
    public static void main(String[] args) {

        char[] arr1=  {'a','b','c'};

        System.out.println(Arrays.toString(f2(arr1,0,4)));
        System.out.println(Arrays.toString(f2(arr1,0,3)));
        System.out.println(Arrays.toString(f2(arr1,0,2)));
        System.out.println(Arrays.toString(f2(arr1,0,1)));
        System.out.println(Arrays.toString(f2(arr1,1,3)));
        System.out.println(Arrays.toString(f2(arr1,1,2)));
        System.out.println(Arrays.toString(f2(arr1,1,1)));
        System.out.println(Arrays.toString(f2(arr1,2,2)));



    }

    private static char[] f2(char[] a, int start, int n) {
        if(a.length< start+n){
            return null;
        }
       char[] arrNew = new char[n];
        int newArrayI= 0;
        for (int i = start; i <a.length; i++) {
           if(newArrayI< n){
               arrNew[newArrayI]=a[i];
               newArrayI++;
           }


        }
        return arrNew;
    }

}
