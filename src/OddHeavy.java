//
//. An array is defined to be odd-heavy if it contains at least one odd element and every odd element
//is greater than every even element. So {11, 4, 9, 2, 8} is odd-heavy because the two odd elements (11 and 9)
//are greater than all the even elements. And {11, 4, 9, 2, 3, 10} is not odd-heavy because the even element 10 is
//greater than the odd element 9. Write a function called isOddHeavy that accepts an integer array and returns
//1 if the array is odd-heavy; otherwise it returns 0. Some other examples: {1} is odd-heavy, {2} is not oddheavy, {1, 1, 1, 1} is odd-heavy, {2, 4, 6, 8, 11} is odd-heavy, {-2, -4, -6, -8, -11} is not odd-heavy.
//If you are programming in Java or C#, the function signature is
//int isOddHeavy(int[ ] a)
//If you are programming in C or C++, the function signature is
//int isOddHeavy(int a[ ], int len)
//where len is the number of elements in the array

public class OddHeavy {
    public static void main(String[] args) {
        int[] arr1={11, 4, 9, 2, 8};
        int[] arr2={11, 4, 9, 2, 3, 10};
        int[] arr3={3, 11, 2, 2};
//        int[] arr4={3, 1, 1, 2, 2,3};
        System.out.println(isOddHevy(arr1));
        System.out.println(isOddHevy(arr2));
        System.out.println(isOddHevy(arr3));


    }

    private static int isOddHevy(int[] arr1) {
        int isOddPresent =0;
        for (int i = 0; i < arr1.length ; i++) {
            if(arr1[i]%2 != 0 ){
                isOddPresent = 1;
                int num =arr1[i];
                if(!isOddGreater(arr1,num)){
                 return 0;
                }
            }

        }
        if(isOddPresent == 0){
            return 0;
        }

        return 1;

    }

    private static boolean isOddGreater(int[] a, int target) {
        for (int i = 0; i < a.length; i++) {
            if(a[i] % 2 == 0 && (target< a[i])){
                return false;
            }

        }
        return true;
    }
}
