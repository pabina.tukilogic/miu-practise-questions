
//Write a function named countMax that returns the number of times that the max
//value occurs in the array. For example, countMax would return 2 if the array is {6. 3,
//        1, 3, 4, 3, 6, 5}because 6 occurs 2 times in the array.
//If you are programming in Java or C#, the function signature is
//int countMax (int[ ] a)
//If you are programming in C or C++, the function signature is
//int countMax (int a[ ], int len) where len is the number of elements in a.

public class CountMax {
    public static void main(String[] args) {
        int [] a1={6,3,1, 3, 4, 3, 6, 5};
        int [] a2={6,7,7,7,8,8,8,8,3,1, 3, 4, 3, 6, 5};
//        int [] a4={256,256,256};
//        int [] a2 ={8,8,8};
//        int [] a3 ={2,2};
        System.out.println(countMax(a1));
        System.out.println(countMax(a2));
//        System.out.println(countMax(a3));
//        System.out.println(countMax(a4));
    }

    private static int countMax(int[] a) {
        int max = 0;
        int maxcount =0;
        for (int i = 0; i < a.length ; i++) {
            if(a[i]>max){
                max = a[i];
            }

        }

        for (int i = 0; i <a.length ; i++) {
           if(a[i] == max ) {
               maxcount++;
           }
        }
        return maxcount;
    }

}
