//
//A Daphne array is defined to be an array that contains at least one odd number and begins and
//ends with the same number of even numbers.
//So {4, 8, 6, 3, 2, 9, 8,11, 8, 13, 12, 12, 6} is a Daphne array because it begins with three even
//numbers and ends with three even numbers and it contains at least one odd number
//The array {2, 4, 6, 8, 6} is not a Daphne array because it does not contain an odd number.
//The array {2, 8, 7, 10, -4, 6} is not a Daphne array because it begins with two even numbers but ends
//with three even numbers.
//Write a function named isDaphne that returns 1 if its array argument is a Daphne array. Otherwise, it
//returns 0.
//If you are writing in Java or C#, the function signature is
//int isDaphne (int[ ] a)
//If you are writing in C or C++, the function signature is
//int isDaphne (int a[ ], int len) where len is the number of elements in the array.


public class DaphneArray {
    public static void main(String[] args) {
        int[] arr1= {4, 8, 6, 3, 2, 9, 8,11, 8, 13, 12, 12, 6};
        int[] arr2= {2, 4, 6, 8, 6};
        int[] arr3= {2, 8, 7, 10, -4, 6};
        int[] arr4= {2, 2, 8, 10, 2, 6};
        System.out.println(isDaphne(arr1));
        System.out.println(isDaphne(arr2));
        System.out.println(isDaphne(arr3));
        System.out.println(isDaphne(arr4));
    }

    private static int isDaphne(int[] a) {
      if(!isContainODD(a)){
          return 0;
      }
      if(!isContainEqualEven(a)){
          return 0;
      }
      return 1;

    }

    private static boolean isContainEqualEven(int[] a) {
        int firsEvenCount = 0;
        int lastEventCount = 0;
        int i =0;
        int j = a.length-1;

        while(a[i]%2 == 0){
           firsEvenCount ++;
           i++;
        }

        while (a[j]%2 == 0){
            lastEventCount ++;
            j--;
        }

     if(firsEvenCount == lastEventCount){
         return true;
     }



     return false;
    }

    private static boolean isContainODD(int[] a) {
        for (int i = 0; i <a.length ; i++) {
            if(a[i] % 2 !=0){
                return  true;
            }

        }
        return false;
    }
}
