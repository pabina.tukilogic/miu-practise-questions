
//a. The array contains even numbers
//b. Let min be the smallest even number in the array.
//c. Let max be the largest even number in the array.
//d. min does not equal max
//e. All numbers between min and max are in the array
//For example {-5, 6, 2, 3, 2, 4, 5, 11, 8, 7} is complete because
//a. The array contains even numbers
//b. 2 is the smallest even number
//c. 8 is the largest even number
//d. 2 does not equal 8
//e. the numbers 3, 4, 5, 6, 7 are in the array.
//Examples of arrays that are not complete are:
//        {5, 7, 9, 13} condition (a) does not hold, there are no even numbers.
//        {2, 2} condition (d) does not hold
//{2, 6, 3, 4} condition (e) does not hold (5 is missing)

//Write a function named isComplete that returns 1 if its array argument is a complete array. Otherwise
//it returns 0.
//If you are writing in Java or C#, the function signature is
//int isComplete (int[ ] a)
//If you are writing in C or C++, the function signature is
//int isComplete (int a[ ], int len) where len is the number of elements in the array.




public class CompleteArray {
    public static void main(String[] args) {
        int[] myarr1 = {5, 7, 9, 13};

        System.out.println("the result is"+isComplete(myarr1));
    }

    private static int isComplete(int[] myarr1) {
        int isevenContain = 0;
        int min = myarr1[0];
        int max = 0;


        for (int i = 0; i < myarr1.length; i++) {
            if(i % 2 == 0){
             isevenContain = 1 ;
            }
           if(myarr1[i] < min){
               min = myarr1[i];
           }
           if(myarr1[i]> max){
               max = myarr1[i];
           }
        }

        int allBetweenNumber = allBetween(myarr1,max,min);

        int myresult = (max != min) && (isevenContain != 0) && (allBetweenNumber != 0) ? 1 : 0;


        return myresult;

    }

    private static int allBetween(int[] myarr1, int max, int min) {
        for (int j = min + 1; j < max; j++) {
            boolean isFound = false;
            for (int num : myarr1) {
                if (num == j) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                return 0;
            }
        }

        return 1;
    }



}
