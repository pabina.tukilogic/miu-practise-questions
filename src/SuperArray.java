
//An Super array is defined to be an array in which each element is greater than sum of
//all elements before that. See examples below:
//        {2, 3, 6, 13} is a Super array. Note that 2 < 3, 2+3 < 6, 2 + 3 + 6 < 13.
//        {2, 3, 5, 11} is a NOT a Super array. Note that 2 + 3 not less than 5.
//Write a function named isSuper that returns 1 if its array argument is a isSuper array,
//otherwise it returns 0.
//If you are programming in Java or C#, the function signature is:
//int isSuper (int [ ] a)


public class SuperArray {
    public static void main(String[] args) {
        int [] a={2, 3, 6, 13};
        int [] a1={2, 3, 5, 11};
        int [] a2={1, 2, 9, 12};
        System.out.println(isSuper(a));
        System.out.println(isSuper(a1));
        System.out.println(isSuper(a2));
    }

    private static int isSuper(int[] a) {
        int sum =0;
        for (int i = 0; i < a.length; i++) {
            if(a[i]< sum || a[i] == sum){
               return 0;
            }
            sum +=a[i];

        }
        return 1;
    }
}
