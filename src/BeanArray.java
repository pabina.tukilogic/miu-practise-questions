//An array is defined to be a Bean array if it meets the following conditions
//a. If it contains a 9 then it also contains a 13.
//b. If it contains a 7 then it does not contain a 16.
//So {1, 2, 3, 9, 6, 13} and {3, 4, 6, 7, 13, 15}, {1, 2, 3, 4, 10, 11, 12} and {3, 6, 9, 5, 7, 13, 6, 17} are
//Bean arrays. The following arrays are not Bean arrays:
//        a. { 9, 6, 18} (contains a 9 but no 13)
//        b. {4, 7, 16} (contains both a 7 and a 16)
//Write a function named isBean that returns 1 if its array argument is a Bean array, otherwise it returns
//0.
//If you are programming in Java or C#, the function signature is
//int isBean (int[ ] a)
//If you are programming in C or C++, the function signature is
//int isBean (int a[ ], int len) where len is the number of elements in the array.
public class BeanArray {
    public static void main(String[] args) {
        int[] arr1= {1, 2, 3, 9, 6, 13,7};
        int[] arr2= {3, 4, 6, 7, 13, 15};
        int[] arr3= {1, 2, 3, 4, 10, 11, 12};
        int[] arr4= {3, 6, 9, 5, 7, 13, 6, 17};
        int[] arr5= { 9, 6, 18};
        int[] arr6= {4, 7, 16};

        System.out.println(isBean(arr1));
        System.out.println(isBean(arr2));
        System.out.println(isBean(arr3));
        System.out.println(isBean(arr4));
        System.out.println(isBean(arr5));
        System.out.println(isBean(arr6));

        //for version 2
        System.out.println("---------------------------------");
        System.out.println(isBeanv2(arr1));
        System.out.println(isBeanv2(arr2));
        System.out.println(isBeanv2(arr3));
        System.out.println(isBeanv2(arr4));
        System.out.println(isBeanv2(arr5));
        System.out.println(isBeanv2(arr6));



    }



    private static int isBeanv2(int[] arr1) {
        int arrLength = arr1.length;
       int isThere9 = 0;
       int isThere13 = 0;
       int isThere7 = 0;
       int isThere16 =0;
       int nothingThere =1;

        for (int i = 0; i < arrLength; i++) {
            if(arr1[i] == 9){
                isThere9 = 1;
                nothingThere =0;
            }
            if (arr1[i] == 13) {
               isThere13 = 1;

            }
            if (arr1[i]== 7){
                isThere7 = 1;
                nothingThere =0;
            }
            if (arr1[i] == 16){
                isThere16 = 1;
            }

        }
        boolean firstcondition =(isThere9==1 && isThere13==1) || isThere9 ==0;
        boolean sencondCondition =(isThere7==1 && isThere16 ==0) || isThere7 ==0;
        return  (firstcondition && sencondCondition) ?1:0;
    }

    private static int isBean(int[] arr1) {
     int arrLength= arr1.length;
        for (int i = 0; i < arrLength ; i++) {
            if(arr1[i]==9){
                for (int j = 0; j <arrLength ; j++) {
                    if(arr1[j] == 13){
                        return 1;
                    }


                }
                return 0;
            }


            if(arr1[i] == 7){
                for (int j = 0; j <arrLength ; j++) {
                    if(arr1[j] == 16){
                        return 0;
                    }

                }
                return 1;

            }


        }
        return 1;
    }








}
