
//An array is said to be dual if it has an even number of elements and each pair of consecutive even and odd elements
//sum to the same value. Write a function named isDual that accepts an array of integers and returns 1 if the array is dual, otherwise it
//returns 0. Examples: {1, 2, 3, 0} is a dual array (because 1+2 = 3+0 = 3), {1, 2, 2, 1, 3, 0} is a dual array (because 1+2 = 2+1 = 3+0 = 3),
//{1, 1, 2, 2}</td> is not a dual array (because 1+1 is not equal to 2+2), {1, 2, 1}</td> <td> is not a dual array (because array does not
//        have an even number of elements), {} is a dual array.
//If you are programming in Java or C#, the function signature is

public class DualArray2 {
    public static void main(String[] args) {
        int[] arr1={1, 2, 3, 0};
        int[] arr2={1, 2, 2, 1, 3, 0};
        int[] arr3={1, 1, 2, 2};
        int[] arr4={1, 2, 1};
        System.out.println(isDualNumber(arr1));
        System.out.println(isDualNumber(arr2));
        System.out.println(isDualNumber(arr3));
        System.out.println(isDualNumber(arr4));
    }

    private static int isDualNumber(int[] a) {
        int i =0;
        int previousSum = 0;
        int latterSum =0;
        if(a.length %2 != 0){
         return 0;
        }
       while (i< a.length-2){
           previousSum = a[i]+a[i+1];
           i +=2;
           latterSum = a[i]+a[i+1];
           if(previousSum != latterSum){
               return 0;
           }
       }
       return 1;
    }
}
