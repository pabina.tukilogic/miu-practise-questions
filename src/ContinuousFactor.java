//An integer is defined to be “continuous factored” if it can be expressed as the product of two or
//more continuous integers greater than 1.
//Examples of “continuous factored” integers are:
//        6 = 2 * 3.
//        60 = 3 * 4 * 5
//        120 = 4 * 5 * 6
//        90 = 9*10
//Examples of integers that are NOT “continuous factored” are: 99 = 9*11, 121=11*11, 2=2, 13=13
//Write a function named isContinuousFactored(int n) that returns 1 if n is continuous factored and 0
//otherwise.



public class ContinuousFactor {
    public static void main(String[] args) {
        System.out.println(isContinuousFactored(6));
        System.out.println(isContinuousFactored(60));
        System.out.println(isContinuousFactored(120));
        System.out.println(isContinuousFactored(90));
        System.out.println(isContinuousFactored(99));
        System.out.println(isContinuousFactored(121));
        System.out.println(isContinuousFactored(13));
        System.out.println(isContinuousFactored(142));
    }

    private static int isContinuousFactored(int n) {

        for (int i = 2; i < n; i++) {
            int product = 1;
            int j = i;
            while (product <= n){
                product *= j;
                j++;

                if (product == n){
                    return 1;
                }
            }

        }

        return 0;

//
//        for (int i = 2; i <n ; i++) {
////            if(n%i == 0 && n%(i+1) != 0){
////                iscontinuous =0;
////            }
//            if(n%i == 0 && n%(i+1) == 0){
//                iscontinuous =1;
//            }
//
//
//        }
//        return iscontinuous;
    }
}
