//An Sub array is defined to be an array in which each element is greater than sum of all elements
//after that. See examples below:
//        {13, 6, 3, 2} is a Sub array. Note that 13 > 2 + 3 + 6, 6 > 3 + 2, 3 > 2.
//        {11, 5, 3, 2} is a NOT a Sub array. Note that 5 is not greater than 3 + 2.
//Write a function named isSub that returns 1 if its array argument is a Sub array, otherwise it returns 0.
//If you are programming in Java or C#, the function signature is:
//int isSub (int [ ] a)
//If you are programming in C or C++, the function signature is:
//int isSub (int a[ ], int len) where len is the number of elements in the array.




public class SubArray {
    public static void main(String[] args) {
        int [] a={13, 6, 3, 2};
        int [] a1={11, 5, 3, 2};
        int [] a2={12,6, 6};
        System.out.println(isSub(a));
        System.out.println(isSub(a1));
        System.out.println(isSub(a2));
    }

    private static int isSub(int[] a) {
        int actualLength = a.length -1;
        for (int i = 0; i < actualLength; i++) {
            int sum =0;
            for (int j = actualLength; j > i ; j--) {
                sum +=a[j];

            }


            if(sum> a[i] || sum == a[i]){
                return 0;
            }

        }
        return 1;
    }
}
