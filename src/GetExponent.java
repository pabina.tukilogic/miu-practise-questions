

// * Write a method named getExponent(n, p) that returns the largest exponent x such that px evenly divides n.
// * If p is <= 1 the method  should return -1.
// * For example, getExponent(162, 3) returns 4 because 162 = 21 * 34, therefore the value of x here is 4.
// * The method signature is  int getExponent(int n, int p)
// * Examples:
// * if   n is and    p is     return                 because
// *      27          3       3                       3*3 divides 27 evenly but 3*4 does not.
// *      28          3       0                       3*0 divides 28 evenly but 3*1 does not.
// *      280         7       1                       7*1 divides 280 evenly but 7*2 does not.
// *      -250        5       3                       5*3 divides -250 evenly but 5*4 does not.
// *      18          1       -1                         if p <=1 the function returns -1.
// *      128         4       3                       4*3 divides 128 evenly but 4*4 does not.



public class GetExponent {
    public static void main(String[] args) {
        System.out.println(getExponentThere(27,3));
        System.out.println(getExponentThere(28,3));
        System.out.println(getExponentThere(128,4));
        System.out.println(getExponentThere(280,7));
        System.out.println(getExponentThere(18,1));

    }

    private static int getExponentThere(int n, int p) {
       int countExpo=0;

        if(p<= 1){
             return  -1;
        }

       while (n%p == 0){
           n = n/p;
           countExpo++;
       }

       return  countExpo;
    }
}
